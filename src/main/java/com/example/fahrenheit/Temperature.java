package com.example.fahrenheit;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet
public class Temperature extends HttpServlet {
    private String message;
    private Converter converter;
    public void init() {
        this.converter = new Converter();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        String title = "Celsius to Fahrenheit";
        out.println("<html><body>");
        out.println("<h1>" + title +  "</h1>");
        out.println("<p>Celsius degrees: \n" + request.getParameter("Celsius"));
        out.println("\n" + "<p>Temperature: " + Double.toString(converter.convert(request.getParameter("Celsius"))));
        out.println("</body></html>");
    }

}