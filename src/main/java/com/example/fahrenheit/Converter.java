package com.example.fahrenheit;

public class Converter {
    public double convert(String celsius){
        double Celsius = Double.parseDouble(celsius);
        return Celsius*1.8+32;
    }
}
